/*****************************************

Encryption application
Copyright 2021 by Gregg Ink 
Released under zlib license

requires files sha256.c and sha256.h
to compile:
gcc -c sha256.c
this will create sha256.o
then compile application:
gcc -o encrypt main.c sha256.o

******************************************/

#include "stdlib.h"
#include "stdint.h"
#include "stdio.h"
#include <sys/stat.h>
#include <unistd.h>

#include "sha256.h"

// most significant digit in 1 byte char
#define TEC_MOST_SIG_BIT 128
#define TEC_2ND_MOST_SIG_BIT 64
#define TEC_3RD_MOST_SIG_BIT 32
#define TEC_4TH_MOST_SIG_BIT 16

/*tec_encrypt:
	this function returns encrypted data
	char *msg is the data you want to encrypt
	int len is the length of msg
	char *pass is a password which will be used to encrypt
	password should not exceed 1012 bytes including NULL character

	the same function is to be used to decrypt
	char *msg is the encrypted data originally returned by the function
	char *pass is the same password used to encrypt the original data

	all usual precautions evidently apply here
	e.g. do not use predictable password
	If you lose the password, there is no way of recovering the data

	potential pitfall:
	the returned encrypted data is not a null-terminated string
	it does possibly contain a null character somewhere in the middle
	the returned cypher is the same length as msg

	This function uses a combination of SHA256 and one-time pad.
	This algorithm is good enough for most uses. Use at your own risk as always.

	free the returned string
*/
char*		tec_encrypt(char *msg, int len, char *pass);

/*tec_string_copy:
	safer alternative to strcpy or even strncpy
	int size is the size of the destination
	these functions guarantee that at most size - 1 bytes will be written to destination plus a NULL character
	this prevents buffer overflows
	this guarantees you get a NULL terminated string in destination (unlike strncpy)
	this function will not cut off the copying in the middle of a UTF8 codepoint when out of space
	returns 1 if all was copied right
	returns 2 if source could not be fully copied
	returns 0 in case of error

	these functions assume char *source is a correctly formatted UTF8 string
*/
int			tec_string_copy(char *destination, char *source, int size);

/*tec_string_concatenate:
tec_string_cat:
	concatenates two strings
	will guarantee a NULL pointer at end of destination even if source is too large to fit
	this function will not cut off the copying in the middle of a UTF8 codepoint when out of space
	returns 0 in case of error
	returns 1 if everything went right
	returns 2 if source was too large
*/
int			tec_string_concatenate(char *destination, char *source, int size);
#define tec_string_cat(X, Y, Z) tec_string_concatenate(X, Y, Z)

/*tec_string_length:
	returns the length of a string in bytes
	check tec_string_utf8_length to know the number of codepoints
	unlike strlen, this function does not segfault when you give it a NULL pointer
	instead it returns zero because, well, you gave it an empty string ;-)
*/
int			tec_string_length(char *str);

/*tec_string_from_int:
	takes an integer and converts it to a string
	writes the result into char* buffer
	returns the number of bytes written (including NULL character)
	returns -1 upon failure
*/
int			tec_string_from_int(int32_t i, char *buffer, int32_t buffer_size);

/*tec_file_get_contents:
	this will return the raw contents of a file in a NULL terminated buffer
	returns NULL in case of error (file does not exist, no read permission, etc.)
	free the returned buffer
*/
char*		tec_file_get_contents(char *path);

/*tec_file_exists:
	returns 1 if file exists
	returns 0 in all other cases ( doesn't exist or error )
*/
int			tec_file_exists(char *path);

/*tec_file_get_size:
	returns the size of the file in bytes
	returns -1 in case of error e.g. file does not exist
*/
int64_t		tec_file_get_size(char *path);


///////////////////////////////////////////////////////////////////////////////

char* tec_encrypt(char *msg, int len, char *pass){

	if(!msg)
		return NULL;
	if(!pass)
		return NULL;
	if(tec_string_length(pass) > 1011)
		return NULL;
	if(len <= 0)
		return NULL;

	unsigned char hash[32];
	int step = 1;
	char to_hash[1024];
	int i = 0;
	char num[12];
	T_SHA256_CTX ctx;
	int k = 0;

	char *cypher = (char *) malloc(sizeof(char) * ( len + 1 ) );
	if(!cypher){
		return NULL;
	}

	while(k < len){
		tec_string_copy(to_hash, pass, 1024);
		tec_string_from_int(step, num, 12);
		tec_string_cat(to_hash, num, 1024);
		step += 1;
		// produce hash
		t_sha256_init(&ctx);
		t_sha256_update(&ctx, (unsigned char *) to_hash, tec_string_length( to_hash));
		t_sha256_final(&ctx, hash);
		i = 0;
		while(k < len && i < 32){
			cypher[k] = msg[k] ^ hash[i];
			i += 1;
			k += 1;
		}
	}

	return cypher;

}//tec_encrypt*/

int tec_string_copy(char *destination, char *source, int size){

	if(!destination)
		return 0;
	if(!source){
		*destination = 0;
		return 1;
	}
	if(size <= 0)
		return 0;

	size -= 1;

	int i = 0;
	while(*source && i < size){
		destination[i] = *source;
		source += 1;
		i += 1;
	}

	// we don't want to cut off the copying in the middle of a UTF8 codepoint
	// firstly check whether the next byte of source is either not present or the start of a codepoint
	if(*source && (*source & TEC_MOST_SIG_BIT) && !(*source & TEC_2ND_MOST_SIG_BIT) ){
		i -= 1;
		// this while loop goes back while we have the 2nd, 3rd or 4th byte of a UTF8 codepoint
		while( (destination[i] & TEC_MOST_SIG_BIT) && !(destination[i] & TEC_2ND_MOST_SIG_BIT) ){
			i -= 1;
		}
		// this goes back from the head of a UTF8 codepoint
		if( (destination[i] & TEC_MOST_SIG_BIT) && (destination[i] & TEC_2ND_MOST_SIG_BIT) ){
			destination[i] = 0;
		}else{
			// should never happen, this would be invalid UTF8
			destination[i] = 0;
			return 0;
		}
	}

	destination[i] = '\0';

	if(*source){
		return 2;
	}else{
		return 1;
	}

}//tec_string_copy*/

int tec_string_concatenate(char *destination, char *source, int size){

	if(!destination)
		return 0;
	if(!source)
		return 0;
	if(!*source)
		return 1;
	if(size < 2)
		return 0;

	int len = tec_string_length(destination);
	if(len > size)
		return 0;
	if(len == size && !*source)
		return 1;

	destination += len;
	size -= 1;

	while(*source && len < size){
		*destination = *source;
		len += 1;
		destination += 1;
		source += 1;
	}

	// we don't want to cut off the copying in the middle of a UTF8 codepoint
	// firstly check whether the next byte of source is either not present or the start of a codepoint
	if(*source && (*source & TEC_MOST_SIG_BIT) && !(*source & TEC_2ND_MOST_SIG_BIT) ){
		destination -= 1;
		// this while loop goes back while we have the 2nd, 3rd or 4th byte of a UTF8 codepoint
		while( (*destination & TEC_MOST_SIG_BIT) && !(*destination & TEC_2ND_MOST_SIG_BIT) ){
			destination -= 1;
		}
		// this goes back from the head of a UTF8 codepoint
		if( (*destination & TEC_MOST_SIG_BIT) && (*destination & TEC_2ND_MOST_SIG_BIT) ){
			*destination = 0;
		}else{
			// should never happen, this would be invalid UTF8
			*destination = 0;
			return 0;
		}
	}else{
		*destination = 0;
	}

	if(*source)
		return 2;
	return 1;

}//tec_string_concatenate*/

int tec_string_length(char *str){

	if(!str)
		return 0;
	if(!*str)
		return 0;

	int len = 0;

#if __x86_64__

	int64_t *str_i = (int64_t *) str;
	int64_t addr = (int64_t) str_i;

	// 64 bit computer
	// ensure str is on 8-byte boundary before using speed-up trick
	while( addr&0x0000000000000007 && *str ){
		len += 1;
		str += 1;
		addr = (int64_t) str;
	}

	if(!*str){
		return len;
	}

	// check for NULL characters, 8 bytes at a time
	// https://graphics.stanford.edu/~seander/bithacks.html#ZeroInWord
	str_i = (int64_t *) str;
	while( !( ( *str_i - 0x0101010101010101 ) & ~(*str_i) & 0x8080808080808080 ) ){
		len += 8;
		str_i += 1;
	}

	str = (char *) str_i;
	while(*str){
		len += 1;
		str += 1;
	}

#else

	int32_t *str32_i = str;
	int32_t addr32 = (int32_t) str32_i;

	// 32 bit computer
	// ensure str is on 4-byte boundary before using speed-up trick
	while( addr32&0x00000003 && *str ){
		len += 1;
		str += 1;
		addr32 = (int32_t) str;
	}

	if(!*str){
		return len;
	}

	// check for NULL characters, 4 bytes at a time
	// https://graphics.stanford.edu/~seander/bithacks.html#ZeroInWord
	str32_i = (int32_t *) str;
	while( !( ( *str32_i - 0x01010101 ) & ~(*str32_i) & 0x80808080 ) ){
		len += 4;
		str32_i += 1;
	}

	str = (char *) str32_i;
	while(*str){
		len += 1;
		str += 1;
	}

#endif

	return len;

}//tec_string_length*/

int tec_string_from_int(int32_t i, char *result, int32_t buffer_size){

	if(!result)
		return -1;

	int tmp = i;
	int len = 1;
	int j = 0;
	int negative = 0;

	if(i < 0){
		negative = 1;
		len += 1;
		tmp *= -1;
	}

	while(tmp > 9){
		tmp /= 10;
		len += 1;
	}

	if(len + 1 > buffer_size){
		result[0] = '\0';
		return -1;
	}

	result[len] = '\0';
	j = len;
	if(negative){
		result[0] = '-';
		i *= -1;
	}
	while(j - negative){
		j -= 1;
		result[j] = i%10 + '0';
		i /= 10;
	}

	return len + 1;	//+1 accounts for NULL character

}//tec_string_from_int*/

char* tec_file_get_contents(char *path){

	if(!path)
		return NULL;

	int64_t size = tec_file_get_size(path);
	if(size == -1)
		return NULL;

	size += 1;
	FILE *fp = fopen(path, "rb");
	if(!fp){
		return NULL;
	}
	char *buffer = (char *) malloc(sizeof(char) * size);
	fread(buffer, sizeof(char), size, fp);
	fclose(fp);
	buffer[size-1] = 0;
	return buffer;

}//tec_file_get_contents*/

int tec_file_exists(char *path){

	if(!path)
		return 0;

	if(access(path, F_OK) != -1){
		return 1;
	}else{
		return 0;
	}

}//tec_file_exists*/

int64_t tec_file_get_size(char *path){

	if(!path)
		return -1;

#ifdef __linux
	struct stat st;
	int error = stat(path, &st);
	if(!error){
		return (int64_t) st.st_size;
	}else{
		return -1;
	}
#endif

#ifdef __WIN32
	BOOL test = FALSE;
	int64_t size;
	HANDLE fp = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	test = GetFileSizeEx(fp, (PLARGE_INTEGER) &size);
	CloseHandle(fp);
	if(test){
		return size;
	}
#endif

	return -1;

}//tec_file_get_size*/

int main(int argc, char **argv){

	if(argc < 3){
		printf("usage: encrypt msg.txt cypher.txt\n");
		return 2;
	}

	char *file_input = argv[1];

	if(!tec_file_exists(file_input)){
		printf("file %s does not appear to exist.\n", file_input);
		return 2;
	}

	int64_t size = tec_file_get_size(file_input);
	char *buffer = tec_file_get_contents(file_input);
	char *cypher = tec_encrypt(buffer, size, "Bene Gesserit");

	FILE *fp = fopen(argv[2], "wb");
	if(!fp){
		printf("failed to open a file for writing\n");
		return 2;
	}
	fwrite(cypher, 1, size, fp);
	fclose(fp);

//	free(buffer);
//	free(cypher);

	return 0;

}//main*/
